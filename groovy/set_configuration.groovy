
def execute() {
    env.getDataOnly = deployConfig['getDataOnly']
    env.checkDataSection = deployConfig['checkDataSection']
    env.tempUploadDir = deployConfig['tempUploadDir']
    if (env.getDataOnly.toBoolean()) {
        sh """
            ${env.WORKSPACE}/scripts/replace.sh "${env.sourceTemplatePackagePath}" "//<getDataOnly>" "getDataOnly = true"
        """
    } else {
        sh """
            ${env.WORKSPACE}/scripts/replace.sh "${env.sourceTemplatePackagePath}" "//<getDataOnly>" "getDataOnly = false"
        """
    }
    if (env.checkDataSection.toBoolean()) {
        sh """
            ${env.WORKSPACE}/scripts/replace.sh "${env.sourceTemplatePackagePath}" "//<checkDataSection>" "checkDataSection = true"
        """
    } else {
        sh """
            ${env.WORKSPACE}/scripts/replace.sh "${env.sourceTemplatePackagePath}" "//<checkDataSection>" "checkDataSection = false"
        """
    }
    if (env.tempUploadDir.toBoolean()) {
        sh """
            ${env.WORKSPACE}/scripts/replace.sh "${env.sourceTemplatePackagePath}" "//<tempUploadDir>" 'tempUploadDir = "/shared/docs/"'
        """
    } else {
        sh """
            ${env.WORKSPACE}/scripts/replace.sh "${env.sourceTemplatePackagePath}" "//<tempUploadDir>" 'tempUploadDir = "/tmp"'
        """
    }
}

return this