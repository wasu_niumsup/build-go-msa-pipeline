
def execute() {
    def elastic = deployConfig['adapters']['elastic']
    if (elastic) {
        def host = deployConfig['adapters']['elastic']['host']
        def port = deployConfig['adapters']['elastic']['port']
        def init = deployConfig['adapters']['elastic']["init"]
        def enable = deployConfig['adapters']['elastic']["enable"]
        if (host) {
            if (init) {
                def variable = "//<adapter.elastic.init>"
                def value = init.replace("#host#", host)
                value = value.replace("#port#", port.toString())
                println("Binding variable " + variable + " with " + value)
                sh """
                    ${env.WORKSPACE}/scripts/replace.sh "${env.mainPackagePath}" "${variable}" "${value}"
                """                              
            } 
            if (enable) {
                def variable = "//<adapter.elastic.enable>"
                def value = enable
                println("Binding variable " + variable + " with " + value)
                sh """
                    ${env.WORKSPACE}/scripts/replace.sh "${env.mainPackagePath}" "${variable}" "${value}"
                """                              
            } 
        }
    }
}

return this