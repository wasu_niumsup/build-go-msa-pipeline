
def execute() {
    def mongodb = deployConfig['adapters']['mongodb']
    if (mongodb) {
        def connection_url = deployConfig['adapters']['mongodb']['connection_url']
        def init = deployConfig['adapters']['mongodb']['init']
        def enable = deployConfig['adapters']['mongodb']["enable"]
        if (connection_url) {
            if (init) {
                def variable = "//<adapter.mongodb.init>"
                def value = init.replace("#connection_url#", connection_url)
                println("Binding variable " + variable + " with " + value)
                sh """
                    ${env.WORKSPACE}/scripts/replace.sh "${env.mainPackagePath}" "${variable}" "${value}"
                """  
            }
            if (enable) {
                def variable = "//<adapter.mongodb.enable>"
                def value = enable
                println("Binding variable " + variable + " with " + value)
                sh """
                    ${env.WORKSPACE}/scripts/replace.sh "${env.mainPackagePath}" "${variable}" "${value}"
                """                              
            } 
        }
    }
}

return this