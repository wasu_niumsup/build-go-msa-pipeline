
def execute() {
    def redis = deployConfig['adapters']['redis']
    if (redis) {
        def host = deployConfig['adapters']['redis']['host']
        def port = deployConfig['adapters']['redis']['port']
        def dbnum = deployConfig['adapters']['redis']['dbnum']
        def init = deployConfig['adapters']['redis']["init"]
        def enable = deployConfig['adapters']['redis']["enable"]
        if (host) {
            if (init) {
                def variable = "//<adapter.redis.init>"
                def value = init.replace("#host#", host)
                value = value.replace("#port#", port.toString())
                value = value.replace("#dbnum#", dbnum.toString())
                println("Binding variable " + variable + " with " + value)
                sh """
                    ${env.WORKSPACE}/scripts/replace.sh "${env.mainPackagePath}" "${variable}" "${value}"
                """                              
            } 
            if (enable) {
                def variable = "//<adapter.redis.enable>"
                def value = enable
                println("Binding variable " + variable + " with " + value)
                sh """
                    ${env.WORKSPACE}/scripts/replace.sh "${env.mainPackagePath}" "${variable}" "${value}"
                """                              
            } 
        } 
    }
}

return this