
def execute() {
    def kafka = deployConfig['adapters']['kafka']
    if (kafka) {
        def bootstrap_servers = deployConfig['adapters']['kafka']['bootstrap_servers']
        def init = deployConfig['adapters']['kafka']["init"]
        def enable = deployConfig['adapters']['kafka']["enable"]
        if (bootstrap_servers) {
            if (init) {
                def variable = "//<adapter.kafka.init>"
                def value = init.replace("#bootstrap_servers#", bootstrap_servers)
                println("Binding variable " + variable + " with " + value)
                sh """
                    ${env.WORKSPACE}/scripts/replace.sh "${env.mainPackagePath}" "${variable}" "${value}"
                """                              
            }
            if (enable) {
                def variable = "//<adapter.kafka.enable>"
                def value = enable
                println("Binding variable " + variable + " with " + value)
                sh """
                    ${env.WORKSPACE}/scripts/replace.sh "${env.mainPackagePath}" "${variable}" "${value}"
                """                              
            } 
        }
    }
}

return this