
def execute() {
    def rpc = deployConfig['adapters']['rpc']
    if (rpc) {
        def name = deployConfig['adapters']['rpc']['name']
        def host = deployConfig['adapters']['rpc']['host']
        def port = deployConfig['adapters']['rpc']['port']
        def timeout = deployConfig['adapters']['rpc']['timeout']
        def init = deployConfig['adapters']['rpc']['init']
        def enable = deployConfig['adapters']['rpc']["enable"]
        if (name) {
            if (init) {
                def variable = "//<adapter.rpc.init>"
                def value = init.replace("#host#", host)
                value = value.replace("#port#", port.toString())
                value = value.replace("#timeout#", timeout.toString())
                value = value.replace("#name#", name)
                println("Binding variable " + variable + " with " + value)
                sh """
                    ${env.WORKSPACE}/scripts/replace.sh "${env.mainPackagePath}" "${variable}" "${value}"
                """  
            }
            if (enable) {
                def variable = "//<adapter.rpc.enable>"
                def value = enable
                println("Binding variable " + variable + " with " + value)
                sh """
                    ${env.WORKSPACE}/scripts/replace.sh "${env.mainPackagePath}" "${variable}" "${value}"
                """                              
            } 
        }
    }
}

return this