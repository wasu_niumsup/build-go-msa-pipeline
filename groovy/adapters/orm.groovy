
def execute() {
    def dbConnectors = deployConfig['adapters']['orm']
    if (dbConnectors) {
        def connectorSize = dbConnectors.size()
        def i = 0
        println(connectorSize)
        for(connector in dbConnectors) {
            println(i)
            def name = connector.name
            def db_type = connector.db_type
            def driver_name = connector.driver_name
            def connstring = connector.connstring
            def timeout = connector.timeout
            def max_idleconn = connector.max_idleconn
            def max_openconn = connector.max_openconn
            def init = connector.init
            def enable = connector.enable
            if (name) {
                if (init) {
                    def variable = "//<adapter.orm.init>"
                    def value = init.replace("#connstring#", connstring)
                    value = value.replace("#name#", name)
                    value = value.replace("#driver_name#", driver_name)
                    value = value.replace("#db_type#", db_type)
                    value = value.replace("#timeout#", timeout.toString())
                    value = value.replace("#max_idleconn#", max_idleconn.toString())
                    value = value.replace("#max_openconn#", max_openconn.toString())
                    
                    if (i < (connectorSize - 1)) {
                        value = value + "<next_line>//<adapter.orm.init>"
                    }
                    println("Binding variable " + variable + " with " + value)
                    sh """
                        ${env.WORKSPACE}/scripts/replace.sh "${env.mainPackagePath}" "${variable}" "${value}"
                    """                              
                } 
                if (enable) {
                    def variable = "//<adapter.orm.enable>"
                    def value = enable.replace("#name#", name)
                    if (i < (connectorSize - 1)) {
                        value = value + "<next_line>//<adapter.orm.enable>"
                    }
                    println("Binding variable " + variable + " with " + value)
                    sh """
                        ${env.WORKSPACE}/scripts/replace.sh "${env.mainPackagePath}" "${variable}" "${value}"
                    """     
                    i = i + 1                         
                } 
            }
        }
    }
}

return this