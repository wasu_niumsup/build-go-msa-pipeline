
def execute() {
    def zipkin = deployConfig['adapters']['zipkin']
    if (zipkin) {
        def service = deployConfig['adapters']['zipkin']['service']
        def hostport = deployConfig['adapters']['zipkin']['hostport']
        def init = deployConfig['adapters']['zipkin']['init']
        if (service) {
            if (init) {
                def variable = "//<adapter.zipkin.init>"
                def value = init.replace("#service#", service)
                value = value.replace("#hostport#", hostport)
                println("Binding variable " + variable + " with " + value)
                sh """
                    ${env.WORKSPACE}/scripts/replace.sh "${env.mainPackagePath}" "${variable}" "${value}"
                """                              
            } 
        } 
    }
}

return this