
def execute() {
    def beegocache = deployConfig['adapters']['beegocache']
    if (beegocache) {
        def host = deployConfig['adapters']['beegocache']['host']
        def port = deployConfig['adapters']['beegocache']['port']
        def dbnum = deployConfig['adapters']['beegocache']['dbnum']
        def key = deployConfig['adapters']['beegocache']['key']
        def init = deployConfig['adapters']['beegocache']["init"]
        def enable = deployConfig['adapters']['beegocache']["enable"]
        if (host) {
            if (init) {
                def variable = "//<adapter.beegocache.init>"
                def value = init.replace("#key#", key)
                value = value.replace("#host#", host)
                value = value.replace("#port#", port.toString())
                value = value.replace("#dbnum#", dbnum.toString())
                println("Binding variable " + variable + " with " + value)
                sh """
                    ${env.WORKSPACE}/scripts/replace.sh "${env.mainPackagePath}" "${variable}" "${value}"
                """                              
            }
            if (enable) {
                def variable = "//<adapter.beegocache.enable>"
                def value = enable
                println("Binding variable " + variable + " with " + value)
                sh """
                    ${env.WORKSPACE}/scripts/replace.sh "${env.mainPackagePath}" "${variable}" "${value}"
                """                              
            } 
        }
    }
}

return this