
def execute() {
    def mqttConnectors = deployConfig['adapters']['mqtt']
    if (mqttConnectors) {
        def connectorSize = mqttConnectors.size()
        def i = 0
        println(connectorSize)
        for(connector in mqttConnectors) {
            println(i)
            def name = connector.name
            def url = connector.url
            def username = connector.username
            def password = connector.password
            def init = connector.init
            def enable = connector.enable
            def enable_subscriber = connector.enable_subscriber
            if (name) {
                if (init) {
                    def variable = "//<adapter.mqtt.init>"
                    def value = init.replace("#url#", url)
                    value = value.replace("#name#", name)
                    value = value.replace("#username#", username)
                    value = value.replace("#password#", password)
                    
                    if (i < (connectorSize - 1)) {
                        value = value + "<next_line>//<enable.mqtt>"
                    }
                    i = i + 1
                    println("Binding variable " + variable + " with " + value)
                    sh """
                        ${env.WORKSPACE}/scripts/replace.sh "${env.mainPackagePath}" "${variable}" "${value}"
                    """                              
                } 
                if (enable) {
                    def variable = "//<adapter.mqtt.enable>"
                    def value = enable
                    println("Binding variable " + variable + " with " + value)
                    sh """
                        ${env.WORKSPACE}/scripts/replace.sh "${env.mainPackagePath}" "${variable}" "${value}"
                    """                              
                }
                if (enable_subscriber) {
                    def variable = "//<enable_subscriber>"
                    def value = enable_subscriber.replace("#url#", url)
                    value = value.replace("#username#", username)
                    value = value.replace("#password#", password)
                    println("Binding variable " + variable + " with " + value)
                    sh """
                        ${env.WORKSPACE}/scripts/replace.sh "${env.mainPackagePath}" "${variable}" "${value}"
                    """                              
                } 
            }
        }
    }
}

return this