
def execute() {
    sh "git clone ssh://git@bitbucket.partsoto.net:7799/${env.sandboxCommonRepo} ${env.moduleRootPath}/${env.sandboxCommonPath}"
    sh "${env.WORKSPACE}/scripts/merge_sandbox.sh ${env.moduleRootPath}/${env.sandboxCommonPath} ${env.sourceTemplatePackagePath}"
    sh """
            ${env.WORKSPACE}/scripts/replace.sh "${env.sourceTemplatePackagePath}" "${sandboxCommonPackage}" "${env.mainPackage}"
    """
}

return this