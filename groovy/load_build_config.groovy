
def execute(config) {
    try {
        parseJson = readJSON text: config
    } catch (Exception e) {
        error("Cannot read json config.\nError:\n${e}")
    }
    try {
        if (parseJson['common']) {
            env.commonRepo = parseJson['common']['repository']
            env.commonPackage = parseJson['common']['package']
            env.commonVersion = parseJson['common']['version']
        }
        if (parseJson['container']) {
            env.containerName = parseJson['container']['name']
            env.imageName = parseJson['container']['image']
            env.dockerInputFile = parseJson['container']['docker_file']
            env.dockerComposeFile = parseJson['container']['compose_file']
            env.imageVersion = parseJson['container']['image_version']
        }
        return parseJson
    } catch (Exception e) {
        println "Cannot load build configuration.\nError:\n${e}"
        error("Cannot load build configuration.\nError:\n${e}")
    }
}

return this