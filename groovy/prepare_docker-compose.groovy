
def execute(dockerVolumeArray) {
    sh """
        ${env.WORKSPACE}/scripts/replace_docker-compose-template.sh "${env.WORKSPACE}/composes" "<container_name>" "${env.containerName}"
    """
    sh """
        ${env.WORKSPACE}/scripts/replace_docker-compose-template.sh "${env.WORKSPACE}/composes" "<container_image>" "${env.imageName}"
    """
    indx = 1
    for(docker_volume in dockerVolumeArray) {
        println("volume")
        def source = docker_volume['source']
        def target = docker_volume['target']
        println("volume1")
        v = "- <volume" + indx + ">"
        mount_volume = '- ' + source + ':' + target
        println(mount_volume)
        println(v)
        sh """
            ${env.WORKSPACE}/scripts/replace_docker-compose-template.sh "${env.WORKSPACE}/composes" "${v}" "${mount_volume}"
        """        
    }
    for (i = indx; i <= 5; i++) {
        v = "- <volume" + i + ">"
        sh """
            ${env.WORKSPACE}/scripts/replace_docker-compose-template.sh "${env.WORKSPACE}/composes" "${v}" ""
        """        
    }

}

return this