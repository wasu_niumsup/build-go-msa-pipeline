
def execute(configFile) {
    def exists = fileExists "${configFile}"
    if (exists) {
        echo "${configFile} found"
    } else {
        error("${configFile} cannot be found")
    }
    try {
        deployConfig = readJSON file: "${configFile}"
    } catch (Exception e) {
        error("Cannot read ${configFile} file.\nError:\n${e}")
    }
    try {
        env.mainPackage = deployConfig['main_package']
        env.moduleDirectory = deployConfig['module_directory']
        env.sourceTemplatePackage = deployConfig['source_template_package']
        env.libModulePath = deployConfig['lib_module_path']
        env.extLibModule = deployConfig['ext_lib_module']
        env.lineToken = deployConfig['line_token']
        env.moduleRootPath = env.WORKSPACE + '/' + env.moduleDirectory
        env.sourceTemplatePackagePath = env.moduleRootPath + '/' + env.sourceTemplatePackage
        env.registryHostUrl = deployConfig['registry_host_url']
        env.privateKey = deployConfig['private_key']
        env.registryUsername = deployConfig['registry_username']
        env.registryPassword = deployConfig['registry_password']

        if (deployConfig['template']) {
            env.templateRepo = deployConfig['template']['repository']
            env.templatePackage = deployConfig['template']['package']
            env.templateVersion = deployConfig['template']['version']
        }
        if (deployConfig['common']) {
            env.commonRepo = deployConfig['common']['repository']
            env.commonPackage = deployConfig['common']['package']
            env.commonVersion = deployConfig['common']['version']
        }
        if (deployConfig['sandbox_common']) {
            env.sandboxCommonRepo = deployConfig['sandbox_common']['repository']
            env.sandboxCommonPath = deployConfig['sandbox_common']['path']
            env.sandboxCommonPackage = deployConfig['sandbox_common']['package']
            env.sandboxCommonVersion = deployConfig['sandbox_common']['version']
        }
        if (deployConfig['container']) {
            env.containerName = deployConfig['container']['name']
            env.imageName = deployConfig['container']['image']
            env.dockerInputFile = deployConfig['container']['docker_file']
            env.dockerComposeFile = deployConfig['container']['compose_file']
            env.imageVersion = deployConfig['container']['image_version']
        }
        return deployConfig
    } catch (Exception e) {
        error("Cannot read ${configFile} property: deployment.files\nError:\n${e}")
    }
}

return this