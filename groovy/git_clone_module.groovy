
def execute(moduleArray) {
    for(module in moduleArray) {
        def moduleName = module['name']
        def modulePackage = module['package']
        def moduleSource = module['source']
        def moduleVersion = module['version']
        println("Clone module source for " + moduleSource)
        sh """
            git clone ${moduleSource} ${env.moduleRootPath}/tmp/${modulePackage}
        """
        println("Merge module source for " + moduleSource)
        sh "${env.WORKSPACE}/scripts/merge.sh ${env.moduleRootPath}/tmp/${modulePackage} ${env.sourceTemplatePackagePath}"
        println("Replace module package for " + moduleSource)
        sh """
            ${env.WORKSPACE}/scripts/replace.sh "${env.sourceTemplatePackagePath}" "${modulePackage}" "${env.mainPackage}"
        """
        sh "rm -rf ${env.moduleRootPath}/tmp"
    }
}

return this