
def execute(moduleArray) {
    for(module in moduleArray) {
        def moduleName = module['name']
        def modulePackage = module['package']
        def moduleSource = module['source']
        def moduleVersion = module['version']
        println("Clone module source for " + moduleSource)
        sh """
            git clone ${moduleSource} ${env.moduleRootPath}
        """
    }
}

return this