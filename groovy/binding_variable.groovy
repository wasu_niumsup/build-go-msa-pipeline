
def execute(bindingVarArray) {
    for(bindingVar in bindingVarArray) {
        def variable = bindingVar.variable
        def value = bindingVar.value
        println("Binding variable " + variable + " with " + value)
        sh """
            ${env.WORKSPACE}/scripts/replace.sh "${env.sourceTemplatePackagePath}" "${variable}" "${value}"
        """
    }
}

return this