#!/bin/sh
cd /go/src/techberry-go/apis/mqtt
go build -o /app techberry-go/apis/mqtt
FILE=/app/mqtt
if [ -f "/app/mqtt" ]; then
    cd /app
    mv mqtt apis_subscriber
fi