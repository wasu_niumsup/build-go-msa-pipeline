#!/bin/sh
name=$1
module=$2
package=$3

echo $name
echo $module
echo $package
cd /go/src/$package
rm -rf go.mod
rm -rf go.sum
ls -la
go build -buildmode=plugin -o $name.so $name.go
ls -la