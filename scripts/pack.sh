#!/bin/sh
echo "Compile and pack microservice"
rm -rf /app/deployed
mkdir -p /app/deployed
cd /go-modules/msa-template
GO111MODULE=on go build -mod=vendor -o apis main.go
cd /app/deployed
cp /go-modules/msa-template/apis .
cp -R //go-modules/msa-template/conf .
#GO111MODULE=off go build -o apis main.go
#bee pack -ba="-mod=vendor"
#rm -rf vendor
#tar -zxvf msa-template.tar.gz
#mv msa-template apis
#rm -rf go.mod
#rm -rf go.sum
#rm -rf README.md