#!/bin/sh

export SOURCE_PATH=$1
export DEST_PATH=$2
export exclude_dir1="./.git/*"
export exclude_file1="bind_input_root.go"
export exclude_file2="service_controller.go"
export exclude_file3="service_accessor.go"
export exclude_file4="go.mod"
export exclude_file5="go.sum"
export exclude_file6="index.go"
echo "Merge script is execute."
cd $SOURCE_PATH
find . -type f \( -iname "*.*" ! -path "$exclude_dir1" ! -name "$exclude_file1" ! -name "$exclude_file2" ! -name "$exclude_file3" ! -name "$exclude_file4" ! -name "$exclude_file5" ! -name "$exclude_file6" \) -print0 -prune -o -depth | cpio -0pdv --quiet $DEST_PATH
