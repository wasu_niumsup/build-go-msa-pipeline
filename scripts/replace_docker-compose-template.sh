#!/bin/sh

sourcePath=$1
stringReplaceFrom=$2
stringReplaceTo=$3

#### Main 
echo "Replace script is execute."
echo $sourcePath
echo $stringReplaceFrom
echo $stringReplaceTo
cd $sourcePath
find . -path './docker-compose-msa-template.yml' -exec sed -i "s?$stringReplaceFrom?$stringReplaceTo?g;s?<next_line>?\r\n\t?g" {} +