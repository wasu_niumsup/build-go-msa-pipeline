#!/bin/sh

sourcePath=$1
stringReplaceFrom=$2
stringReplaceTo=$3

#### Main 
echo "Replace script is execute."
echo $sourcePath
echo $stringReplaceFrom
echo $stringReplaceTo
cd $sourcePath
find . -type f -not -path './vendor/*' -name "*" -not -path './.git/*' -not -path './go.*' -not -path './gomod_download.sh' -not -path './.gitignore' -not -path './README.md' -exec sed -i "s?$stringReplaceFrom?$stringReplaceTo?g;s?<next_line>?\r\n\t?g" {} +