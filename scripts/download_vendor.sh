#!/bin/sh
GO111MODULE=on go clean -cache -modcache
cd /go-modules/msa-template
GO111MODULE=on go mod tidy
GO111MODULE=on go mod vendor