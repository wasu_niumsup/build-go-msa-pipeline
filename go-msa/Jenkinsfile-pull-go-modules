#!/usr/bin/env groovy

def deployConfig
def bindingVarArray

def notifyLINE(token, status) {
    def jobName = env.JOB_NAME + ' ' + env.BRANCH_NAME
    def buildNo = env.BUILD_NUMBER
      
    def url = 'https://notify-api.line.me/api/notify'
    def message = "${jobName} Build #${buildNo} ${status} \r\n"
    sh "curl ${url} -H 'Authorization: Bearer ${token}' -F 'message=${message}'"
}


pipeline {
    agent any
    options {
        // using the Timestamper plugin we can add timestamps to the console log
        timestamps()
        // Set timeout for build pipeline job.
        timeout(time: 20, unit: 'MINUTES')
    } 
    parameters {
        string(name: 'MODULE_CONFIG', defaultValue: 'deploy_config/go/pull-go-modules.json', description: 'Module Configuration Ex. deploy_config/go/pull-go-modules.json')
    }
    stages {
        stage('Load Configuration') {
            steps {
                script {
                    def moduleConfig = "${params['MODULE_CONFIG']}"
                    def module = load "${env.WORKSPACE}/groovy/load_module_config.groovy"
                    deployConfig = module.execute(moduleConfig)
                    bindingVarArray = deployConfig['binding_var']
                }
            }
        }
        stage('Clean Caching Modules') {
            steps {
                sh "rm -rf ${env.libModulePath}"
            }
        }
        stage('Clone Golang Template') {
            steps {
                sh "mkdir -p ${env.libModulePath}/${env.templatePackage}"
                sh "git clone -b ${env.templateVersion} --single-branch ssh://git@bitbucket.partsoto.net:7799/${env.templateRepo} ${env.libModulePath}/${env.templatePackage}"
            }
        }
        stage('Clone Common Library') {
            steps {                 
                sh "mkdir -p ${env.libModulePath}/${env.commonPackage}"
                sh "git clone -b ${env.commonVersion} --single-branch ssh://git@bitbucket.partsoto.net:7799/${env.commonRepo} ${env.libModulePath}/${env.commonPackage}"
                sh "git clone -b v1.12.3 --single-branch ssh://git@bitbucket.partsoto.net:7799/gom-lib/astaxie_beego.git ${env.libModulePath}/astaxie_beego"
                sh "git clone -b v1.3.3 --single-branch ssh://git@bitbucket.partsoto.net:7799/gom-lib/mitchellh_mapstructure.git ${env.libModulePath}/mitchellh_mapstructure"
                sh "git clone -b v7.0.22 --single-branch ssh://git@bitbucket.partsoto.net:7799/gom-lib/olivere_elastic_v7.git ${env.libModulePath}/olivere_elastic_v7"
                sh "git clone -b v1.4.3 --single-branch ssh://git@bitbucket.partsoto.net:7799/gom-lib/mongodb_mongo_driver.git ${env.libModulePath}/mongodb_mongo_driver"
            }
        }  
        stage('Copy To Module Root') {
            steps {
                echo "Home workspace path : ${env.WORKSPACE}"
                sh "rm -rf ${moduleRootPath}"
                sh "mkdir -p ${moduleRootPath}"
                sh "cp -Rp ${env.libModulePath}/* ${moduleRootPath}"
            }
        }
        stage('Build & Download') {
            steps {
                sh "docker build --no-cache -f ${env.dockerInputFile} -t ${env.imageName} -t ${env.imageName} ."
                sh "docker run --name ${env.containerName} ${env.imageName}"
                sh "docker cp ${env.containerName}:/go-modules/msa-template ${env.libModulePath}"
                sh "docker cp ${env.containerName}:/go-modules/techberry-go ${env.libModulePath}"
                sh "docker ps -f name=${env.containerName} -q | xargs --no-run-if-empty docker container stop"
                sh "docker container ls -a -fname=${env.containerName} -q | xargs -r docker container rm"
            }
        } 
    }

    post {
        success{
            notifyLINE("${env.lineToken}", "succeed")
        }
        failure{
            notifyLINE("${env.lineToken}", "failed")
        }
    }
}