#!/usr/bin/env groovy

def deployConfig
def bindingVarArray
def moduleArray
def notifyLINE(token, status) {
    def jobName = env.JOB_NAME + ' ' + env.BRANCH_NAME
    def buildNo = env.BUILD_NUMBER
      
    def url = 'https://notify-api.line.me/api/notify'
    def message = "${jobName} Build #${buildNo} ${status} \r\n"
    sh "curl ${url} -H 'Authorization: Bearer ${token}' -F 'message=${message}'"
}

// Prepare, Build, Deploy, Test, Release Process Stages
pipeline {
    agent any
    options {
        // using the Timestamper plugin we can add timestamps to the console log
        timestamps()
        // Set timeout for build pipeline job.
        timeout(time: 20, unit: 'MINUTES')
    } 
    parameters {
        string(name: 'MODULE_CONFIG', defaultValue: 'deploy_config/go/apis-msa-auth.json', description: 'Module Configuration Ex. deploy_config/go/apis-msa-auth.json')
    }
    stages {
        stage('Prepare') {
            stages {
                stage('Load Configuration') {
                    steps {
                        script {
                            def moduleConfig = "${params['MODULE_CONFIG']}"
                            def module = load "${env.WORKSPACE}/groovy/load_configuration.groovy"
                            deployConfig = module.execute(moduleConfig)
                            bindingVarArray = deployConfig['binding_var']
                            moduleArray = deployConfig['modules'] 
                        }
                    }
                }
                stage('Prepare Modules Directory') {
                    steps {
                        echo "Home workspace path : ${env.WORKSPACE}"
                        sh "rm -rf ${moduleRootPath}"
                    }
                }
                stage('Copy Module Template') {
                    steps {
                        sh "cp -Rp ${env.libModulePath} ${env.WORKSPACE}"
                        sh "mkdir -p ${env.mainPackagePath}"
                        sh "mv ${env.sourceTemplatePackagePath}/* ${env.mainPackagePath}"
                        sh "rm -rf ${env.sourceTemplatePackagePath}"
                        sh """
                            ${env.WORKSPACE}/scripts/replace.sh "${env.mainPackagePath}" "${sourceTemplatePackage}" "${env.mainPackage}"
                        """
                    }
                }
            }
        }
        stage('Clone') {
            stages {
                stage('Git Clone Module') {
                    steps {
                        script {
                            def module = load "${env.WORKSPACE}/groovy/git_clone_module.groovy"
                            module.execute(moduleArray)
                        }
                    }
                }
                stage('Git Clone Sandbox Common') { 
                    steps {
                        script {
                            def module = load "${env.WORKSPACE}/groovy/git_clone_sandbox_common.groovy"
                            module.execute()
                        }
                    }
                }
                stage('Binding Variable') {
                    steps {
                        script {
                            def module = load "${env.WORKSPACE}/groovy/binding_variable.groovy"
                            module.execute(bindingVarArray)
                        }
                    }
                }
            }
        }
        stage('Init/Enable Connectors') {
            stages {
                stage('Init/Enable Mongodb Connector') {
                    steps {
                        script {
                            def module = load "${env.WORKSPACE}/groovy/adapters/mongodb.groovy"
                            module.execute()
                        }
                    }
                }
                stage('Init/Enable RPC Connector') {
                    steps {
                        script {
                            def module = load "${env.WORKSPACE}/groovy/adapters/rpc.groovy"
                            module.execute()
                        }
                    }
                }
                stage('Init/Enable Kafka Connector') {
                    steps {
                        script {
                            def module = load "${env.WORKSPACE}/groovy/adapters/kafka.groovy"
                            module.execute()
                        }
                    }
                }
                stage('Init/Enable Elastic Connector') {
                    steps {
                        script {
                            def module = load "${env.WORKSPACE}/groovy/adapters/elastic.groovy"
                            module.execute()
                        }
                    }
                }
                stage('Init/Enable Elastic Logger') {
                    steps {
                        script {
                            def module = load "${env.WORKSPACE}/groovy/adapters/elastic_logger.groovy"
                            module.execute()
                        }
                    }
                }
                stage('Init/Enable Redis Connector') {
                    steps {
                        script {
                            def module = load "${env.WORKSPACE}/groovy/adapters/redis.groovy"
                            module.execute()
                        }
                    }
                }
                stage('Init/Enable Beego Cache') {
                    steps {
                        script {
                            def module = load "${env.WORKSPACE}/groovy/adapters/beego_cache.groovy"
                            module.execute()
                        }
                    }
                }
                stage('Init/Enable ORM Connector') {
                    steps {
                        script {
                            def module = load "${env.WORKSPACE}/groovy/adapters/orm.groovy"
                            module.execute()
                        }
                    }
                }
                stage('Init/Enable MQTT Connector') {
                    steps {
                        script {
                            def module = load "${env.WORKSPACE}/groovy/adapters/mqtt.groovy"
                            module.execute()
                        }
                    }
                }
                stage('Init/Enable Zipkin Connector') {
                    steps {
                        script {
                            def module = load "${env.WORKSPACE}/groovy/adapters/zipkin.groovy"
                            module.execute()
                        }
                    }
                }
                stage('Set Configuration') {
                    steps {
                        script {
                            def module = load "${env.WORKSPACE}/groovy/set_configuration.groovy"
                            module.execute()
                        }
                    }
                }
            }
        }
        stage('Build') {
            stages {
                stage('Stop Container') {
                    steps {
                        sh "docker ps -f name=${env.containerName} -q | xargs --no-run-if-empty docker container stop"
                    }
                }
                stage('Remove Container') {
                    steps {
                        sh "docker container ls -a -fname=${env.containerName} -q | xargs -r docker container rm"
                    }
                }
                stage('Remove Image') {
                    steps {
                        sh """
                            docker image ls \
                                | awk '{ print \$1,\$2,\$3 }' \
                                | grep ${imageName} \
                                | awk '{print \$3 }' \
                                | xargs -I {} docker image rm {}
                            """
                    }
                }
                stage('Build Image') {
                    steps {
                        sh "docker build --build-arg app_user=${env.appUser} --build-arg app_key=${env.appKey} -f ${env.dockerInputFile} -t ${env.imageName} ."
                    }
                }
            }
        }
        stage('Deploy') {
            stages {
                stage('Deploy Container') {
                    steps {
                        sh "export DOCKER_HOST=unix:///var/run/docker.sock"
                        sh "docker-compose -f ./composes/go/${env.dockerComposeFile} up -d"
                    }
                }
            }
        }
    }

    post {
        success{
            notifyLINE("${env.lineToken}", "succeed")
        }
        failure{
            notifyLINE("${env.lineToken}", "failed")
        }
    }
}